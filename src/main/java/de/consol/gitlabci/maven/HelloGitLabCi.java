package de.consol.gitlabci.maven;

public class HelloGitLabCi
{
    public static String sayHello()
    {
        String hello = "Hello GitLab CI!";
        System.out.println("#################");
        System.out.println(hello);
        System.out.println("#################");
        System.out.println("#################");
        System.out.println("#################");
        return hello;
    }
}
