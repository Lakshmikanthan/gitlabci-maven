package de.consol.gitlabci.maven;
import org.testng.Assert;
import org.testng.Assert;
import org.testng.annotations.Test;

/**
 * Unit test for simple HelloGitLabCi.
 */
public class HelloGitLabCiTest {
    @Test
    public void testSayHello(){
        Assert.assertEquals(HelloGitLabCi.sayHello(),"Hello GitLab CI!");
    }
}
